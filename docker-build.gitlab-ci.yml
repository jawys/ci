# See: https://docs.gitlab.com/ee/ci/yaml/

include:
  - defaults.gitlab-ci.yml
  - snippets.gitlab-ci.yml

variables:
  IMAGE_NAME_TAG: ${IMAGE_NAME}:${IMAGE_TAG}
  IMAGE_NAME: ${CI_REGISTRY_IMAGE}/${WORKDIR}
  IMAGE_TAG: ref-${CI_COMMIT_REF_SLUG}
  PLATFORMS: linux/amd64
  WORKDIR: .

build:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:24.0.6-cli
  services:
    - name: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:24.0.6-dind
      alias: docker
  script:
    - !reference [.snippets, setup_cargo_auth]
    - !reference [.snippets, setup_npm_auth]
    - !reference [.snippets, docker_login]
    - docker buildx create --use
    - docker buildx build "${WORKDIR}"
      --cache-from=type=registry,ref="${IMAGE_NAME_TAG}"
      --cache-from=type=registry,ref="${IMAGE_NAME_TAG%%:*}"
      --cache-to=type=inline,mode=max
      --platform="${PLATFORMS}"
      --provenance=false
      --pull
      --push
      --secret=id=cargo-credentials,src=/usr/local/cargo/credentials.toml
      --secret=id=npmrc,src=.npmrc
      --tag="${IMAGE_NAME_TAG}"
      --tag="${IMAGE_NAME}":g"${CI_COMMIT_SHORT_SHA}" # as in git describe --tags
  rules:
    - if: $GITLAB_USER_LOGIN =~ /_bot_/
      variables:
        CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX: docker.io
        CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX: docker.io
    - if: $CI_COMMIT_REF_NAME
  interruptible: true
  needs: []

# FIXME: Remove in v3 everything below
release:
  stage: .pre
  script: noop
  rules:
    - when: never
release [deprecated]:
  stage: .pre
  image: alpine:latest
  script:
    - ISSUE_DESCRIPTION="${ISSUE_DESCRIPTION//$'\n'/$'\n\n'}"
    - |-
      echo -e "\e[31m
      ${ISSUE_DESCRIPTION}
      \e[0m"
    - apk add -Uq glab && glab version
    - |-
      # If issue with same title exists, ping the user; if not, create new issue
      export CHECK_UPDATE=0
      export GITLAB_REPO="${CI_PROJECT_PATH}"
      export GITLAB_TOKEN="${GITLAB_TOKEN}"
      export NO_PROMPT=1
      found_issues="$( glab issue list -F ids --search "${ISSUE_TITLE}" )"
      if [[ -n "${found_issues}" ]]; then
        glab issue note "${found_issues}" --message "Ping @${GITLAB_USER_LOGIN}"
      else
        glab issue create --assignee "${GITLAB_USER_LOGIN},jawys" \
          --title "${ISSUE_TITLE}" --description "${ISSUE_DESCRIPTION}"
      fi
      exit 42
  rules:
    - if: $IS_DOCKER_RELEASE_INCLUDED
      when: never
    - if: $IS_SEMANTIC_RELEASE_INCLUDED && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  variables:
    ISSUE_TITLE: Include 'docker-release.gitlab-ci.yml' in CI
    ISSUE_DESCRIPTION: |-
      - The `release` job from `docker-build.gitlab-ci.yml` is deprecated
      - Please include `docker-release.gitlab-ci.yml` in `${CI_CONFIG_PATH}`
      - Branch: `${CI_COMMIT_BRANCH}`
      - Job: ${CI_JOB_URL}
